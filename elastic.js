require('dotenv').config()

var Storage = require('./storage');
var storage = new Storage();

const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: process.env.ASTROMETRY_ELASTIC_HOST || 'http://elasticsearch:9200' })

const index = process.env.ASTROMETRY_ELASTIC_INDEX || 'astrometry';

// conditionally create index:
client.indices.exists({
    index: index
}).then(d => {
    if (d.body) {
        console.log("elasticsearch index already exists")
    } else {
        client.indices.create({
            index: index
        }).then(() => {
            console.log("created elasticserach index!")
        }).catch(err => {
            console.log("error creating elasticsearch index!")
            console.log(err)
        });
    }
});

class Elastic {

    client = client;
    index = index;

    constructor() {
    }

    /**
     * put the initial document in elasticsearch
     * @param {*} jobId 
     */
    create(jobId, solveArgs = null, mimeType = null, tags=[],description='') {
        return this.client.index({
            id: jobId,
            index: this.index,
            body: {
                id: jobId,
                original_file: mimeType,
                time: {
                    submitted: new Date().toISOString()
                },
                succeeded: null,
                solve_args: solveArgs,
                description: description,
                tags: {
                    user: tags,
                    machine: []
                }
            }
        })
    }

    status(jobId, stage, status) {
        return this.client.update({
            id: jobId,
            index: this.index,
            body: {
                doc: {
                    time: {
                        [stage]: {
                            [status]: new Date().toISOString()
                        }
                    }
                }
            }
        });
    }

    /**
     * retrieve the job document
     * @param {*} jobId 
     */
    info(jobId) {
        return this.client.getSource({
            id: jobId,
            index: this.index
        })
    }

    /**
     * list job ids
     */
    list(params = {}) {
        return this.client.search({
            index: this.index,
            body: {
                size: params?.size || 20,
                sort: [
                    {
                        [params?.sort || "time.submitted"]: params?.order || "desc"
                    }
                ],
                query: {
                    match_all: {}
                },
                _source: {
                    includes: ["id", "time"]
                }
            }
        })
            .then(data => {
                return data.body.hits.hits.map(x => x._source);
            })
            .catch(err => {
                throw err
            })
    }

    /**
     * add the calibration results to the es document
     * @param {*} jobId 
     */
    index_results(jobId) {
        return storage.getCalibration(jobId).then(doc => {
            return this.client.update({
                id: jobId,
                index: this.index,
                body: {
                    doc: doc
                }
            })
        })
        .catch(err => {
            console.log(err);
            throw err;
        })
    }
}

module.exports = Elastic;