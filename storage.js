require("dotenv").config();
var AWS = require("aws-sdk");

var s3 = new AWS.S3({
  apiVersion: "2006-03-01",
  // check for agent-specific S3 endpoint, then revert to the compute container endpoint, then revert to AWS:
  // -> this can be useful if storage is routed to differently from the agent than from the compute cluster
  endpoint:
    process.env.ASTROMETRY_AGENT_S3_ENDPOINT_URL ||
    process.env.ASTROMETRY_S3_ENDPOINT_URL ||
    "s3.amazonaws.com",
  accessKeyId: process.env.ASTROMETRY_AGENT_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.ASTROMETRY_AGENT_AWS_SECRET_ACCESS_KEY,
  s3ForcePathStyle: true, // needed with minio?
  signatureVersion: "v4",
});

var astrometry_bucket = process.env.ASTROMETRY_BUCKET || "astrometry";

s3.headBucket({ Bucket: astrometry_bucket })
  .promise()
  .then((data) => {
    console.log("Astrometry bucket exists.");
  })
  .catch((err) => {
    console.log("Creating astrometry bucket...");
    s3.createBucket({ Bucket: astrometry_bucket })
      .promise()
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        console.log(err);
        throw err;
      });
  });

class Storage {
  astrometry_bucket = astrometry_bucket;
  s3 = s3;

  constructor() {}

  /**
   * upload original user file
   * @param {*} file
   */
  uploadOriginal(file) {
    const upload_key = ["upload", file.md5, file.name].join("/");
    var params = {
      Body: file.data,
      Bucket: this.astrometry_bucket,
      Key: upload_key,
      ContentType: file.mimetype,
    };
    return this.s3
      .putObject(params)
      .promise()
      .then((data) => {
        return {
          key: upload_key,
        };
      })
      .catch((err) => {
        throw err;
      });
  }

  /**
   * download image
   * @param {*} jobId
   * @param {*} image
   */
  getImage(jobId, image) {
    switch (image) {
      case "original":
        var name = "original.jpg";
        break;
      case "objs":
        var name = "field-objs.png";
        break;
      case "ngc":
        var name = "field-ngc.png";
        break;
      case "indx":
        var name = "field-indx.png";
        break;
      default:
        var name = "oirignal";
    }
    var params = {
      Bucket: this.astrometry_bucket,
      Key: ["jobs", jobId, name].join("/"),
    };
    return this.s3.getObject(params).promise();
  }

  /**
   * return consistently-formatted key to the status object
   * @param {*} jobId
   */
  statusKey(jobId) {
    return ["jobs", jobId, "agent.status"].join("/");
  }

  /**
   * return consistently-formatted key to the calibration object
   * @param {*} jobId
   */
  calibrationKey(jobId) {
    return ["jobs", jobId, "calibration.json"].join("/");
  }

  /**
   * return consistently-formatted key to the object containing stars in the image
   * @param {*} jobId
   */
  objectsInFieldKey(jobId) {
    return ["jobs", jobId, "objectsinfield.json"].join("/");
  }

  /**
   * return consistently-formatted key to the log file object
   * @param {*} jobId
   */
  logKey(jobId) {
    return ["jobs", jobId, "solve.log"].join("/");
  }

  /**
   * return consistently-formatted key to the upload parameters object
   * @param {*} jobId
   */
  uploadParamsKey(jobId) {
    return ["jobs", jobId, "uploadParams.json"].join("/");
  }

  /**
   * write an agent status object to the specified job
   * @param {*} jobId
   * @param {*} status
   */
  setStatus(jobId, stage, message) {
    var status = {
      stage: stage,
      status: message,
      timestamp: new Date().toISOString(),
    };
    var params = {
      Body: JSON.stringify(status),
      Bucket: this.astrometry_bucket,
      Key: this.statusKey(jobId),
    };
    return this.s3
      .putObject(params)
      .promise()
      .then((data) => {
        status.data = data;
        return status;
      })
      .catch((err) => {
        throw err;
      });
  }

  /**
   * retrieve the current job status
   * @param {*} jobid
   */
  getStatus(jobId) {
    var params = {
      Bucket: this.astrometry_bucket,
      Key: this.statusKey(jobId),
    };
    return this.s3
      .getObject(params)
      .promise()
      .then((data) => {
        var status = JSON.parse(data.Body.toString("utf-8"));
        return status;
      })
      .catch((err) => {
        throw err;
      });
  }

  /**
   * save upload parameters to S3, just in case elasticsearch tanks
   * @param {*} jobId 
   */
  putUploadParams(jobId,uploadParams) {
    var params = {
      Body: JSON.stringify(uploadParams),
      Bucket: this.astrometry_bucket,
      Key: this.uploadParamsKey(jobId),
    };
    return this.s3.putObject(params).promise();
  }

  /**
   * retrieve the current job logs
   * @param {*} jobid
   */
  getLogs(jobId) {
    var params = {
      Bucket: this.astrometry_bucket,
      Key: this.logKey(jobId),
    };
    return this.s3
      .getObject(params)
      .promise()
      .then((data) => {
        var logs = data.Body.toString("utf-8");
        return logs;
      })
      .catch((err) => {
        throw err;
      });
  }

  /**
   * retreive the calibration json file
   * @param {*} jobId
   */
  getCalibration(jobId) {
    var doc = {
      succeeded: true,
    };
    return this.s3
      .getObject({
        Bucket: this.astrometry_bucket,
        Key: this.calibrationKey(jobId),
      })
      .promise()
      .then((data) => {
        doc.calibration = JSON.parse(data.Body.toString("utf-8"));
        return this.s3
          .getObject({
            Bucket: this.astrometry_bucket,
            Key: this.objectsInFieldKey(jobId),
          })
          .promise()
          .then((data) => {
            doc.tags = {
              machine: JSON.parse(data.Body.toString("utf-8")),
            };
            return doc;
          });
      })
      .catch((err) => {
        console.log(err);
        throw err;
      });
  }
}

module.exports = Storage;
