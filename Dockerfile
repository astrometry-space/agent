FROM node:14-alpine

ADD . .

RUN npm install

ENTRYPOINT [ "npm", "start" ]
