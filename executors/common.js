require('dotenv').config()
const { v4: uuidv4 } = require('uuid');

class Common {
    
    // container image:
    image = process.env.ASTROMETRY_AGENT_DOCKER_IMAGE || 'registry.gitlab.com/astrometry-space/compute:latest';

    // default solve args:
    solve_args = {
        timestamp: null,
        z: 2
    }

    // default container environment:
    environment = [
        "HUMAN_LOGS=1", // don't really care about json logging at this point?
        "AWS_ACCESS_KEY_ID=" + (process.env.ASTROMETRY_AGENT_AWS_ACCESS_KEY_ID || 'minio'),
        "AWS_SECRET_ACCESS_KEY=" + (process.env.ASTROMETRY_AGENT_AWS_SECRET_ACCESS_KEY || 'minio123'),
        "AWS_DEFAULT_REGION=" + (process.env.ASTROMETRY_AGENT_AWS_DEFAULT_REGION || 'us-east-1'),
        "ASTROMETRY_S3_ENDPOINT_URL=" + (process.env.ASTROMETRY_S3_ENDPOINT_URL || 'http://s3.amazonaws.com'),
        "ASTROMETRY_AGENT_ENDPOINT=" + (process.env.ASTROMETRY_AGENT_ENDPOINT || 'http://agent:3000/agent/'),
        "ASTROMETRY_BUCKET=" + process.env.ASTROMETRY_BUCKET
    ]

    
    constructor() {
        // console.log(this.environment);
    }

    /**
     * genereate a unique job ID and secret token
     * @param {*} input_path 
     */
    generateJobId() {
        var id = [uuidv4()].join('_')
        var token = [uuidv4()].join('_')
        return [id, token]
    }

    /**
     * return array of environment variables for the container
     * @param {*} req 
     * @param {*} job_id 
     */
    createEnvironment(params,job_id,token=null,solve_args=null) {
        const solve_args_json = JSON.stringify(solve_args || this.solve_args);
        var env = this.environment.slice();
        env.push("ASTROMETRY_JOB_ID=" + job_id);
        env.push("ASTROMETRY_INPUT_FILE="+params.image);
        env.push("ASTROMETRY_SOLVE_ARGS="+solve_args_json);
        env.push("ASTROMETRY_TOKEN="+token);
        console.log(env);
        return env;
    }

}

module.exports = Common

