require("dotenv").config();

var Common = require("./common");
var common = new Common();

var Storage = require("../storage");
var storage = new Storage();

var Docker = require("dockerode");
var docker = new Docker({
  socketPath: "/var/run/docker.sock",
  version: "v1.25",
});

// this is necessary to allow swarm nodes to pull from a private registry:
if (process.env.REGISTRY_USER && process.env.REGISTRY_TOKEN) {
  console.log("Using docker registry credentials on swarm nodes.");
  var registryconfig = {
    serveraddress: common.image.split("/")[0],
    email: "",
    username: process.env.REGISTRY_USER,
    password: process.env.REGISTRY_TOKEN,
  };
} else {
  console.log("No docker registry credentials provided.");
  var registryconfig = null;
}

// this could be used to account for scheduling delay, swarm api responsiveness, etc.
// services created less than this ammount of time into the past will not be killed.
const minimum_service_life_seconds = 60;

// filter object is used to return services associated with astrometry:
const list_services_options = {
  filters: {
    label: ["astrometry.job"],
  },
};

class Swarm {
  /**
   * return a list of all astrometry jobs from the swarm, most-recent first:
   */
  list() {
    return docker.listServices(list_services_options).then((services) => {
      var sorted_services = services.sort((a, b) => {
        return new Date(b.CreatedAt) - new Date(a.CreatedAt);
      });
      return sorted_services.map((x) => x.Spec.Name);
    });
  }

  /**
   * flush (likely) completed jobs from the swarm
   */
  flush() {
    var results = {
      deleted: [],
      kept: [],
    };
    return docker.listServices(list_services_options).then((services) => {
      // for each service, create a promise that will resolve when checking the service is complete:
      var service_promises = services.map((x) => {
        return new Promise((resolve, reject) => {
          var s = docker.getService(x.ID);
          s.inspect().then((service_details) => {
            // determine how long the service has existed:
            var created_at = new Date(service_details.CreatedAt);
            var time_since_creation = (new Date() - created_at) / 1000.0;
            // summary information for this service that will be included in the response:
            var Q = {
              id: service_details.ID,
              time_since_creation: time_since_creation,
              reason: null,
            };
            // get container associated with this service, if they exist:
            docker
              .listContainers({
                filters: {
                  label: [
                    "astrometry.job.id=" +
                      service_details.Spec.Labels["astrometry.job.id"],
                  ],
                },
              })
              .then((containers) => {
                if (containers.length > 0) {
                  Q.reason = "containers found";
                  results.kept.push(Q);
                  resolve();
                } else if (time_since_creation < minimum_service_life_seconds) {
                  Q.reason =
                    "no containers found, but has not reached minimum lifespan";
                  results.kept.push(Q);
                  resolve();
                } else if (time_since_creation > minimum_service_life_seconds) {
                  Q.reason =
                    "no containers found, and exceeded minimum lifespan";
                  results.deleted.push(Q);
                  s.remove();
                  resolve();
                }
              });
          });
        });
      });
      // once all the services have been checked, return a summary:
      return Promise.all(service_promises).then((D) => {
        return results;
      });
    });
  }

  /**
   * delete the specified job, return an array of service IDs matching the job id
   * @param {*} jobId
   */
  delete(jobId, token = null) {
    const opts = {
      filters: {
        label: ["astrometry.job.id=" + jobId],
      },
    };
    var results = [];
    return docker.listServices(opts).then((services) => {
      var service_promises = services.map((x) => {
        return new Promise((resolve, reject) => {
          results.push(x.ID);
          var s = docker.getService(x.ID);
          s.inspect()
            .then((data) => {
              if (data.Spec.Labels?.["astrometry.job.token"] == token) {
                s.remove();
                resolve();
              } else {
                resolve();
                console.warn("invalid job token!");
              }
            })
            .catch((err) => {
              reject(err);
            });
        });
      });

      return Promise.all(service_promises)
        .then((D) => {
          return results;
        })
        .catch((err) => {
          throw err;
        });
    });
  }

  /**
   * submit a new astrometry job to the swarm as a service
   */
  submit(params) {
    const [job_id, job_token] = common.generateJobId();

    const solve_args = params?.solve_args;

    storage.setStatus(job_id, "create", "received");

    const labels = {
      "astrometry.job": "1",
      "astrometry.job.id": job_id,
      "astrometry.job.token": job_token,
    };

    const service_opts = {
      Name: job_id,
      Labels: labels,
      TaskTemplate: {
        ContainerSpec: {
          Image: common.image,
          Labels: labels,
          Env: common.createEnvironment(params, job_id, job_token, solve_args),
          Mounts: [
            {
              ReadOnly: true,
              Source:
                process.env.ASTROMETERY_SWARM_INDEX_VOLUME ||
                "astrometry_index",
              Target: "/usr/share/astrometry",
              Type: "Volume",
            },
          ],
        },
        RestartPolicy: {
          Condition: "none",
          MaxAttempts: 0,
        },
      },
      Mode: {
        Replicated: {
          Replicas: 1,
        },
      },
      Networks: [
        {
          Target: process.env.ASTROMETRY_SWARM_NETWORK || "astrometry",
          Aliases: [job_id],
        },
      ],
    };

    if (registryconfig) {
      var create_service_promise = docker.createService(
        registryconfig,
        service_opts
      );
    } else {
      var create_service_promise = docker.createService(service_opts);
    }

    return create_service_promise
      .then((service) => {
        storage.setStatus(job_id, "create", "job secheduled");
        return {
          job_id: job_id,
          service_name: job_id,
          service_id: service.id,
          solve_args: solve_args,
        };
      })
      .catch((err) => {
        console.log(err);
      });
  }

  /**
   * return service logs
   * @param {*} jobId
   */
  logs(jobId, since = 0) {
    var s = docker.getService(jobId);
    return s.logs({
      stdout: true,
      stderr: true,
      since: since,
      tail: "all",
    });
  }
}

module.exports = Swarm;
