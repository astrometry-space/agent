require('dotenv').config()

var Common = require('./common');
var common = new Common();

var Storage = require('../storage');
var storage = new Storage();

class new_executor_name {

    /**
     * return a list of all astrometry jobs, most-recent first:
     */
    list() {
    };

    /**
     * flush (likely) completed jobs
     */
    flush() {
    };

    /**
     * delete the specified job, return an array of service IDs matching the job id
     * @param {*} jobId 
     */
    delete(jobId) {
    }

    /**
     * submit a new astrometry job
     */
    submit(params) {
    };

    /**
     * return job logs
     * @param {*} jobId 
     */
    logs(jobId,since=0) {
    }

}

module.exports = new_executor_name;