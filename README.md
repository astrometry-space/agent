# Agent

Creates astrometry compute jobs.

## Get Started - Docker Swarm

1) Have a Docker Swarm

2) Make sure you [have minio up and running](https://gitlab.com/astrometry-space/minio)

3) Make sure your `astrometry_index`  docker volume exists and has data [as described here](https://gitlab.com/astrometry-space/compute)

4) Clone this repo, and boot it up:
  ```bash
  git clone https://gitlab.com/astrometry-space/agent
  cd agent
  npm install
  nodemon
  ```
5) Add a file to minio at `/original/demo.jpg`.  This will be the star field you are solving.

6) [Go to astrometry.postman.co](https://astrometry.postman.co/collections/5295852-05daaba7-4ced-4fd3-a01b-cf4555f0d4d9?version=latest&workspace=b7b398eb-5d88-4c50-8724-053d287e3d95) to try submitting jobs.

