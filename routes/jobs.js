var express = require('express');
var router = express.Router();

var Elastic = require('../elastic');
var elastic = new Elastic();

var Storage = require('../storage');
var storage = new Storage();


/**
 * list of job ids
 */
router.get('/list', function (req, res, next) {
  elastic.list(req.query)
  .then( id_list => {
    res.send(id_list);
  })
  .catch( err => {
    next(err);
  })
});

/**
 * get full es document for job
 */
router.get('/:jobId/info', function (req, res, next) {
  const jobId = req.params.jobId;
  elastic.info(jobId)
  .then( info => {
    res.send(info.body);
  })
  .catch( err => {
    next(err);
  })
});

/**
 * get the calibration results for a job
 */
router.get('/:jobId/calibration', function (req, res, next) {
  const jobId = req.params.jobId;
  storage.getCalibration(jobId)
  .then( cal => {
    res.send(cal);
  })
  .catch( err => {
    next(err);
  })
});

/**
 * get the logs a job
 */
router.get('/:jobId/logs', function (req, res, next) {
  const jobId = req.params.jobId;
  storage.getLogs(jobId)
  .then( logs => {
    res.send(logs);
  })
  .catch( err => {
    next(err);
  })
});

/**
 * get an image from the job
 */
router.get('/:jobId/image/:image?', function (req, res, next) {
  const jobId = req.params.jobId;
  const image = req.params.image || 'original';
  storage.getImage(jobId,image)
  .then( I => {
    res.writeHead(200,{'Content-Type':I.ContentType});
    res.write(I.Body, 'binary');
    res.send();
  })
  .catch( err => {
    next(err);
  })
});

module.exports = router;
