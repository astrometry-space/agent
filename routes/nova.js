var express = require('express');
var router = express.Router();
require('dotenv').config()

/**
 *  A mock implementation of http://astrometry.net/doc/net/api.html
 * 
 *  This will behave like nova.astrometry.net, but run on a distributed cluster
 */

/**
 * mock login endpoint
 */
router.post('/login', function (req, res, next) {
  var mock_success = {
    status: 'success',
    message: 'authenticated user: anonymous',
    session: 'fake'
  };
  res.send(mock_success);
});

/**
 * submit image from url
 */
router.post('/url_upload', function (req, res, next) {
  console.log(req.body)
  var mock_success = {
    status: 'success',
    subid: 12345, // integer, submission number
    hash: 'fake_url_contents_hash' // sha-1 hash of the url contents
  };
  res.send(mock_success);
});

/**
 * file upload multipart
 */
router.post('/upload', function (req, res, next) {
  console.log(req.body)
  var mock_success = {
    status: 'success',
    subid: 12345, // integer, submission number
    hash: 'fake_file_contents_hash' // sha-1 hash of the file contents
  };
  res.send(mock_success);
});

/**
 * submission status
 */
router.get('/submissions/:submissionId', function (req, res, next) {
  const submissionId = Number(req.params.submissionId)
  console.log("Submission ID: ", submissionId)
  var mock_success = {
    processing_started: "2016-03-29 11:02:11.967627",
    job_calibrations: [[1493115, 785516]],
    jobs: [1493115],
    processing_finished: "2016-03-29 11:02:13.010625",
    user: 1,
    user_images: [1051223]
  };
  res.send(mock_success);
});

/**
 * job status
 */
router.get('/jobs/:jobId', function (req, res, next) {
  const jobId = Number(req.params.jobId)
  console.log("Job ID: ", jobId)
  var mock_success = {
    status: "success"
  };
  res.send(mock_success);
});

/**
 * job calibrations
 */
router.get('/jobs/:jobId/calibration', function (req, res, next) {
  const jobId = Number(req.params.jobId)
  console.log("Job ID: ", jobId)
  var mock_success = {
    parity: 1.0,
    orientation: 105.74942079091929,
    pixscale: 1.0906710701159739,
    radius: 0.8106715896625917,
    ra: 169.96633791366915,
    dec: 13.221011585315143
  };
  res.send(mock_success);
});

/**
 * job info: all results
 */
router.get('/jobs/:jobId/info', function (req, res, next) {
  const jobId = Number(req.params.jobId)
  console.log("Job ID: ", jobId)
  var mock_success = {
    status: "success",
    original_filename: "Leo Triplet-1.jpg",
    machine_tags: ["NGC 3628", "M 66", "NGC 3627", "M 65", "NGC 3623"],
    objects_in_field: ["NGC 3628", "M 66", "NGC 3627", "M 65", "NGC 3623"],
    tags: ["NGC 3628", "M 66", "NGC 3627", "M 65", "NGC 3623"],
    callibration: {
      parity: 1.0,
      orientation: 105.74942079091929,
      pixscale: 1.0906710701159739,
      radius: 0.8106715896625917,
      ra: 169.96633791366915,
      dec: 13.221011585315143
    }
  };
  res.send(mock_success);
});

module.exports = router;
