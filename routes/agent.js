var express = require("express");
var router = express.Router();
require("dotenv").config();

var Storage = require("../storage");
var storage = new Storage();

var Elastic = require("../elastic");
var elastic = new Elastic();

// for determining mime type of submission files:
const got = require("got");
const FileType = require("file-type");
const { request } = require("express");

// get MIME type of remote images:
async function getUrlMime(url) {
  const stream = got.stream(url);
  return await FileType.fromStream(stream);
}
// for file uploads:
async function getBufferMime(buffer) {
  return await FileType.fromBuffer(buffer);
}

// pick which type of executor to use:
const executor_type = process.env.ASTROMETRY_AGENT_EXECUTOR || "swarm";
if (executor_type.toLowerCase() == "swarm") {
  console.log("Executor type: ", executor_type.toUpperCase());
  var Swarm = require("../executors/swarm");
  var executor = new Swarm();
} else if (executor_type.toLowerCase() == "ecs") {
  throw Error(
    "AWS Elastic Container Service (ECS) not supported yet.  Get to work!"
  );
} else if (["kubernetes", "k8s"].indexOf(executor_type.toLowerCase()) >= 0) {
  throw Error("Kubernetes (k8s) not supported yet.  Get to work!");
} else {
  throw Error(
    "no valid executor specified in environment variable: ASTROMETRY_AGENT_EXECUTOR"
  );
}

/**
 * list all jobs on the cluster, regardless of state
 */
router.get("/list", function (req, res, next) {
  executor
    .list()
    .then((jobs) => {
      res.send(jobs);
    })
    .catch((err) => {
      next(err);
    });
});

/**
 * remove all (probably) complete jobs from the cluster
 */
router.post("/flush", function (req, res, next) {
  executor
    .flush()
    .then((D) => {
      res.send(D);
    })
    .catch((err) => {
      next(err);
    });
});

/**
 * remove the specified job id
 */
router.post("/delete/:jobId", function (req, res, next) {
  const jobId = req.params.jobId;
  const jobToken = req.body?.token;
  executor
    .delete(jobId,jobToken)
    .then((D) => {
      if (req.body?.solved) {
        console.log("indexing results for " + jobId);
        elastic
          .index_results(jobId)
          .then((index_results) => {
            D.indexed_reults = index_results;
            res.send(D);
          })
          .catch((err) => {
            next(err);
          });
      } else {
        res.send(D);
      }
    })
    .catch((err) => {
      next(err);
    });
});

/**
 * submit a job to the cluster for processing
 */
router.post("/submit", function (req, res, next) {
  executor
    .submit(req.body)
    .then((submission) => {
      getUrlMime(req.body.image).then((mimeType) => {
        if (!mimeType) {
          console.log("did not detect mime type!");
          mimeType = {
            ext: null,
            mime: null,
          };
        }
        // add the original file name to the mimeType:
        mimeType.name = req.body.image;
        mimeType.upload = false;
        elastic
          .create(
            submission.job_id,
            submission.solve_args,
            mimeType,
            req.body?.tags,
            req.body?.description
          )
          .then((es_data) => {
            res.send(submission);
          });
      });
    })
    .catch((err) => {
      next(err);
    });
});

/**
 * submit a job to the cluster for processing
 * multi-part form:
 *  image: a file
 *  solve_args: a JSON string holding solve_args
 */
router.post("/submit/upload", function (req, res, next) {
  var params = {};
  if ("solve_args" in req.body) {
    try {
      params.solve_args = JSON.parse(req.body.solve_args);
    } catch (err) {
      console.log(err);
      next("Could not parse solve args JSON from multi-part from data");
    }
  } else console.log("No solve options provided.");

  if ( "tags" in req.body ) {
    var tags = JSON.parse(req.body.tags);
  } else {
    var tags = []
  }

  console.log(tags);

  if (req.files?.image) {
    storage
      .uploadOriginal(req.files.image)
      .then((data) => {
        params.image = data.key;
        getBufferMime(req.files.image.data)
          .then((mimeType) => {
            if (!mimeType) {
              mimeType = { ext: null, mime: null };
            }
            // add the original file name to the mimeType:
            mimeType.name = req.files.image.name;
            mimeType.upload = true;
            mimeType.upload_key = data.key;
            executor
              .submit(params)
              .then((submission) => {
                elastic
                  .create(
                    submission.job_id,
                    params.solve_args,
                    mimeType,
                    tags,
                    req.body?.description || ''
                  )
                  .then((es_data) => {
                    res.send(submission);
                  })
                  .catch((err) => {
                    console.log(err);
                    next(err);
                  });
              })
              .catch((err) => {
                console.log(err);
                next(err);
              });
          })
          .catch((err) => {
            console.log(err);
            next(err);
          });
      })
      .catch((err) => {
        console.log(err);
        next(err);
      });
  } else {
    res.statusCode(500).send("Input file not provided!");
  }
});

/**
 * update a job status
 */
router.post("/status/:jobId/:stage/:status", function (req, res, next) {
  storage.setStatus(req.params.jobId, req.params.stage, req.params.status);
  elastic
    .status(req.params.jobId, req.params.stage, req.params.status)
    .then((data) => {
      // console.log(data);
      res.send(data);
    })
    .catch((err) => {
      next(err);
    });
});

/**
 * retreive a job status
 */
router.get("/status/:jobId", function (req, res, next) {
  var jobId = req.params.jobId;
  storage
    .getStatus(jobId)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next(err);
    });
});

/**
 * retreive job logs
 */
router.get("/logs/:jobId", function (req, res, next) {
  var jobId = req.params.jobId;
  // attempt to parse date:
  var since = Number(req.query.since) || 0;
  // useful for testing UI log streaming:
  if (req.query?.mock) {
    res.send({
      time: Date.now() / 1000,
      logs: String(Date.now() / 1000) + ": logs abound",
    });
  } else {
    executor
      .logs(jobId, since)
      .then((logs) => {
        // include the current unix time stamp, the UI can then reference this:
        const log_time = Date.now() / 1000;
        res.send({
          time: log_time,
          logs: String(logs),
        });
      })
      .catch((err) => {
        next(err);
      });
  }
});

/**
 * index the specified job results
 */
router.post("/index/:jobId", function (req, res, next) {
  elastic
    .index_results(req.params.jobId)
    .then((data) => {
      res.send({ message: "indexing results in es" });
    })
    .catch((err) => {
      next(err);
    });
});

module.exports = router;
