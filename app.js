var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('dotenv').config()
const fileUpload = require('express-fileupload');
var cors = require('cors')
var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(fileUpload());

// allow the use of CORS, if configured:
if ( (process.env.CORS_ENABLED || '0') == '1' ) {
  console.log('using CORS middleware');
  app.use(cors());
} else {
  console.log('disabling CORS middleware');
}


// if this is activated as an agent, load the agent routes:
if (process.env.AGENT_ENABLED == '1') {
  var agentRouter = require('./routes/agent');
  app.use('/agent/', agentRouter);
}

var novaRouter = require('./routes/nova');
app.use('/nova/', novaRouter);

var jobRouter = require('./routes/jobs');
app.use('/job/', jobRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // return the error
  res.status(err.status || 500);
  res.send(err);
});

module.exports = app;
